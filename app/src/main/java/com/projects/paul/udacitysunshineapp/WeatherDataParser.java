package com.projects.paul.udacitysunshineapp;

import android.util.Log;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

/**
 * Created by Paul on 2014-10-17.
 */
public class WeatherDataParser {

    public static double getMaxTemperatureForDay(String weatherJsonStr, int dayIndex) throws JSONException {

        JSONObject weatherJsonObject = new JSONObject(weatherJsonStr);

        JSONArray dayArray = weatherJsonObject.getJSONArray("list");
        JSONObject dayObject = dayArray.getJSONObject(dayIndex);
        JSONObject tempObject = dayObject.getJSONObject("temp");
        double maxTemp = tempObject.getDouble("max");

        Log.v("JSON_PARSING", dayArray.toString());
        Log.v("JSON_PARSING", dayObject.toString());
        Log.v("JSON_PARSING", tempObject.toString());
        Log.v("JSON_PARSING", Double.toString(maxTemp));

        return maxTemp;
    }
}
